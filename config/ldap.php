<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/11/14
 * Time: 3:09 PM
 */
return [
    'ldap_proxy'         => Smorken\LdapAuth\Proxy\LdapQueryProxy::class,
    'ldap_provider'      => LdapQuery\Opinionated\ActiveDirectory::class,
    'provider_options'   => [
        'bind_filter'    => env('LDAP_ACCOUNT_SUFFIX', '%s@domain.edu'),
        'host'           => env('LDAP_DOMAIN_CONTROLLER', 'ldap.domain.edu'),
        'base_dn'        => env('LDAP_BASE_DN', 'ou=group,dc=domain,dc=org'),
        'bind_user'      => env('LDAP_ADMIN_USERNAME', ''),
        'bind_password'  => env('LDAP_ADMIN_PASSWORD', ''),
        'client_options' => [
            'start_tls' => env('LDAP_START_TLS', false),
            'ssl'       => env('LDAP_SSL', true),
        ],
    ],
    'load_groups'        => false,
    'username_attribute' => env('LDAP_USERNAME_ATTRIBUTE', 'samaccountname'),
    'id_attribute'       => env('LDAP_ID_ATTRIBUTE', 'uid'),
    'group_attribute'    => 'memberOf',
    'maptouser'          => [],
];
