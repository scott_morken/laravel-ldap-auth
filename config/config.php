<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/11/14
 * Time: 3:09 PM
 */
return array(

    /*
    |--------------------------------------------------------------------------
    | Users
    |--------------------------------------------------------------------------
    |
    | Configuration specific to the user management component of Ldap-Auth.
    |
    */

    'users' => array(

        'model' => Smorken\LdapAuth\Users\Eloquent\User::class,

        'provider' => Smorken\LdapAuth\Providers\Ldap::class,

        /*
        |--------------------------------------------------------------------------
        | Login Attribute
        |--------------------------------------------------------------------------
        |
        | If you're using the "eloquent" model and extending the base Eloquent
        | model, you can override the login attribute used by the model
        |
        */

        'login_attribute' => 'username',
    ),

);
