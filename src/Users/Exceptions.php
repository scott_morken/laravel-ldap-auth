<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/11/14
 * Time: 12:54 PM
 */

namespace Smorken\LdapAuth\Users;

use Illuminate\Support\MessageBag;

/**
 * @package Smorken\LdapAuth\Users
 * Borrowed from Cartalyst Sentry 2.0
 * @link http://cartalyst.com
 */
class LoginRequiredException extends \UnexpectedValueException
{

}

class PasswordRequiredException extends \UnexpectedValueException
{

}

class UserAlreadyActivatedException extends \RuntimeException
{

}

class UserNotFoundException extends \OutOfBoundsException
{

}

class UserNotActivatedException extends \RuntimeException
{

}

class UserExistsException extends \UnexpectedValueException
{

}

class InvalidCredentialsException extends UserNotFoundException
{

}

class AttributeValidationException extends \UnexpectedValueException
{

    /**
     * @var MessageBag
     */
    protected $errors;

    public function __construct(
        MessageBag $errors,
        $message = "Validation Error",
        $code = 0,
        \Exception $previous = null
    ) {
        $this->errors = $errors;
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return MessageBag
     */
    public function getErrors()
    {
        return $this->errors;
    }
}
