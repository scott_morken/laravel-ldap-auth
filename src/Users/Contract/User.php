<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/4/15
 * Time: 7:14 AM
 */

namespace Smorken\LdapAuth\Users\Contract;

/**
 * Interface User
 * @package Smorken\LdapAuth\Users\Contract
 *
 * @property string $id 32 chars
 * @property string $username 32 chars, unique
 * @property string $first_name 32 chars
 * @property string $last_name 32 chars
 * @property string $email 100 chars
 * @property string|null $password 64 chars Required for compatibility with Laravel Auth
 * @property string|null $remember_token 64 chars Required for compatibility with Laravel Auth
 */
interface User extends \Smorken\Auth\User\Contracts\Models\User
{

    public function getProxyAttributes();

    public function getProxyAttribute($key);

    public function setProxyAttribute($key, $value);

    public function setProxyAttributes($attributes);
}
