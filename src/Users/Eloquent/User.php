<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/11/14
 * Time: 12:44 PM
 */

namespace Smorken\LdapAuth\Users\Eloquent;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableInterface;
use Illuminate\Database\Eloquent\Model;
use Smorken\Auth\Exceptions\AuthDisplayableException;
use Smorken\LdapAuth\Users\LdapAuthUserInterface;
use Smorken\LdapAuth\Users\LoginRequiredException;
use Smorken\LdapAuth\Users\Validation\ValidatesUser;

class User extends \Smorken\Auth\User\Models\Eloquent\User implements \Smorken\LdapAuth\Users\Contract\User
{

    protected $proxy_attributes = [];

    protected $appends = ['groups'];

    protected $groups = [];

    public function getGroupsAttribute()
    {
        return $this->groups;
    }

    public function setGroupsAttribute($value)
    {
        $this->groups = $value;
    }

    public function save(array $options = [])
    {
        if (!$this->getLogin()) {
            throw new AuthDisplayableException("Login [{$this->getLoginField()}] not found.");
        }
        $this->validate($this->getAttributes(), $this->rules());
        return parent::save($options);
    }

    public function setProxyAttribute($key, $value)
    {
        $this->proxy_attributes[$key] = $value;
    }

    public function setProxyAttributes($attributes)
    {
        $this->proxy_attributes = $attributes;
    }

    public function getProxyAttributes()
    {
        return $this->proxy_attributes;
    }

    public function getProxyAttribute($key)
    {
        if (isset($this->proxy_attributes[$key])) {
            return $this->proxy_attributes[$key];
        }
    }
}
