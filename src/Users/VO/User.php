<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/30/14
 * Time: 2:12 PM
 */

namespace Smorken\LdapAuth\Users\VO;

use Illuminate\Auth\Authenticatable;
use Smorken\Auth\Exceptions\AuthDisplayableException;
use Smorken\LdapAuth\Users\Validation\ValidatesUser;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableInterface;
use Smorken\LdapAuth\Users\AttributeValidationException;
use Smorken\LdapAuth\Users\LoginRequiredException;

class User extends \Smorken\Auth\User\Models\VO\User implements \Smorken\LdapAuth\Users\Contract\User
{

    protected $proxy_attributes = [];

    protected $attributes = [
        'id'             => null,
        'first_name'     => null,
        'last_name'      => null,
        'username'       => null,
        'email'          => null,
        'password'       => null,
        'remember_token' => null,
        'groups'         => [],
    ];

    public function setProxyAttribute($key, $value)
    {
        $this->proxy_attributes[$key] = $value;
    }

    public function setProxyAttributes($attributes)
    {
        $this->proxy_attributes = $attributes;
    }

    public function getProxyAttributes()
    {
        return $this->proxy_attributes;
    }

    public function getProxyAttribute($key)
    {
        if (isset($this->proxy_attributes[$key])) {
            return $this->proxy_attributes[$key];
        }
    }

    public function save(array $options = [])
    {
        $key = 'user_' . md5($this->id);
        if (!$this->getLogin()) {
            throw new AuthDisplayableException("Login [{$this->getLoginField()}] not found.");
        }
        $this->validate($this->getAttributes(), $this->rules());
        session()->put($key, serialize($this->getAttributes()));
        return $this;
    }
}
