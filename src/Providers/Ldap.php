<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/11/14
 * Time: 12:38 PM
 */

namespace Smorken\LdapAuth\Providers;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;
use Smorken\Auth\Exceptions\AuthDisplayableException;
use Smorken\Auth\Exceptions\AuthException;

class Ldap implements UserProvider
{

    /**
     * storage model representing the user
     * @var string $model
     */
    protected $model = \Smorken\LdapAuth\Users\Eloquent\User::class;

    /**
     * @var \Smorken\LdapAuth\Proxy\ProxyInterface
     */
    protected $proxy;

    protected $proxySetup = false;

    protected $cache;

    public function __construct($proxy, $model = null, $cache = null)
    {
        if (isset($model)) {
            $this->model = $model;
        }
        $this->setCache($cache);
        $this->setProxy($proxy);
    }

    public function setProxy($proxy)
    {
        $this->proxy = $proxy;
    }

    protected function setupProxy()
    {
        $m = $this->createModel();
        $this->proxy()->setLoginField($m->getLoginField());
        $this->proxy()->setIdField($m->getKeyName());
    }

    public function proxy()
    {
        if ($this->proxySetup === false) {
            $this->proxySetup = true;
            $this->setupProxy();
        }
        return $this->proxy;
    }

    public function createModel()
    {
        $class = $this->getClassName($this->model);
        return new $class;
    }

    protected function getClassName($cls)
    {
        return '\\' . ltrim($cls, '\\');
    }

    public function getCache()
    {
        return $this->cache;
    }

    public function setCache($cache)
    {
        $this->cache = $cache;
    }

    /**
     * Retrieve a user by their unique identifier.
     *
     * @param  mixed $identifier
     * @return Authenticatable|null
     * @throws AuthException
     */
    public function retrieveById($identifier)
    {
        if ($this->getCache()) {
            $key = 'ldap/rid/' . $identifier;
            return $this->getCache()->remember(
                $key,
                5,
                function () use ($identifier) {
                    $user = $this->userFromId($identifier);
                    if ($user) {
                        $user->setValidationFactory(null);
                    }
                    return $user;
                }
            );
        }
        return $this->userFromId($identifier);
    }

    protected function userFromId($identifier)
    {
        $model = $this->createModel();
        if (!$user = $model->newQuery()->find(strtolower($identifier))) {
            $ldapinfo = $this->proxy()->findById($identifier);
            if (!$ldapinfo) {
                throw new AuthException("A user could not be found with ID [$identifier].");
            }
            $user = $this->createUserFromLdap($ldapinfo);
        }
        return $user;
    }

    /**
     * @param $username
     * @return Authenticatable|null
     * @throws AuthException
     */
    public function retrieveByUsername($username)
    {
        $model = $this->createModel();
        $loginName = $model->getLoginField();
        if (!$user = $model->newQuery()->where($loginName, '=', strtolower($username))->first()) {
            $ldapinfo = $this->proxy()->findByUsername($username);
            if (!$ldapinfo) {
                throw new AuthException("A user could not be found with username [$username].");
            }
            $user = $this->createUserFromLdap($ldapinfo);
        }
        return $user;
    }

    /**
     * Retrieve a user by the given credentials.
     *
     * @param  array $credentials
     * @return Authenticatable|null
     * @throws AuthDisplayableException
     */
    public function retrieveByCredentials(array $credentials)
    {
        $model = $this->createModel();
        $loginName = $model->getLoginField();
        if (!array_key_exists($loginName, $credentials)) {
            throw new AuthDisplayableException("Login attribute [$loginName] was not provided.", "Username missing.");
        }
        $ldapinfo = $this->proxy()->findByCredentials($credentials);
        if (!$ldapinfo) {
            throw new AuthDisplayableException(
                "The credentials provided did not match any users.",
                "Invalid username and/or password."
            );
        }
        if (!$user = $model->newQuery()->where($loginName, '=', strtolower($credentials[$loginName]))->first()) {
            $user = $this->createUserFromLdap($ldapinfo);
        } else {
            $user = $this->updateUserFromLdap($user, $ldapinfo);
        }
        return $user;
    }

    /**
     * Validate a user against the given credentials.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable|null $user
     * @param  array $credentials
     * @return bool
     */
    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        return $this->proxy()->authenticate($credentials);
    }

    public function retrieveByToken($identifier, $token)
    {
        $model = $this->createModel();
        $identifier = strtolower($identifier);
        return $model->newQuery()
                     ->where($model->getKeyName(), $identifier)
                     ->where($model->getRememberTokenName(), $token)
                     ->first();
    }

    /**
     * @param \Illuminate\Contracts\Auth\Authenticatable $user
     * @param string $token
     */
    public function updateRememberToken(Authenticatable $user, $token)
    {
        $user->setAttribute($user->getRememberTokenName(), $token);

        $user->save();
    }

    public function createUserFromLdap($ldapinfo)
    {
        $groups = config('smorken/ldap-auth::ldap.load_groups', false);
        $converted = $this->proxy()->convertToUserAttributes($ldapinfo, $groups);
        $user = $this->create($converted);
        $user->setProxyAttributes($this->proxy()->getProxyAttributes($ldapinfo));
        return $user;
    }

    public function updateUserFromLdap($user, $ldapinfo)
    {
        $groups = config('smorken/ldap-auth::ldap.load_groups', false);
        $converted = $this->proxy()->convertToUserAttributes($ldapinfo, $groups);
        $dirty = false;
        foreach ($converted as $key => $value) {
            $userval = $user->$key;
            if ($userval != $value) {
                $dirty = true;
                $user->$key = $value;
            }
        }
        if ($dirty) {
            $user->save();
        }
        $user->setProxyAttributes($this->proxy()->getProxyAttributes($ldapinfo));
        return $user;
    }

    /**
     * Creates a user.
     *
     * @param array $data
     * @internal param array $credentials
     * @return \Smorken\LdapAuth\Users\Contract\User
     */
    public function create(array $data)
    {
        $user = $this->createModel();
        $user->fill($data);
        $user->password = 'LDAP';
        $user->save();
        return $user;
    }

    public function find($criteria = [])
    {
        $collections = $this->proxy()->find($criteria);
        $results = [];
        $modelclass = $this->getClassName($this->model);
        $groups = config('smorken/ldap-auth::ldap.load_groups', false);
        foreach ($collections as $coll) {
            $results[] = new $modelclass($this->proxy()->convertToUserAttributes($coll, $groups));
        }
        return $results;
    }
}
