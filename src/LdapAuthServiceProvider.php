<?php namespace Smorken\LdapAuth;

use Illuminate\Auth\AuthServiceProvider;
use Illuminate\Contracts\Cache\Repository;
use Illuminate\Support\Facades\Auth;
use Smorken\LdapAuth\Contracts\Provider;
use Smorken\LdapAuth\Contracts\Proxy;

class LdapAuthServiceProvider extends AuthServiceProvider
{

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->bootConfig();
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        Auth::provider(
            'ldap',
            function ($app, array $config) {
                return $app[Provider::class];
            }
        );
    }

    protected function bootConfig()
    {
        $package_path_conf = __DIR__ . '/../config/config.php';
        $app_path_conf = config_path('/vendor/smorken/ldap-auth/config.php');
        $this->mergeConfigWith($package_path_conf, $app_path_conf, 'smorken/ldap-auth::config');
        $package_path_ldap = __DIR__ . '/../config/ldap.php';
        $app_path_ldap = config_path('/vendor/smorken/ldap-auth/ldap.php');
        $this->mergeConfigWith($package_path_ldap, $app_path_ldap, 'smorken/ldap-auth::ldap');
        $this->publishes(
            [
                $package_path_conf => $app_path_conf,
                $package_path_ldap => $app_path_ldap,
            ],
            'config'
        );
    }

    protected function mergeConfigWith($package_path, $app_path, $key)
    {
        if (file_exists($app_path)) {
            $app_config = require $app_path;
        } else {
            $app_config = [];
        }
        $package_config = require $package_path;
        $this->app['config']->set($key, array_merge($package_config, $app_config));
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->bindProxy();
        $this->bindUserProvider();
    }

    protected function bindProxy()
    {
        $this->app->bind(
            Proxy::class,
            function ($app) {
                $cls = $app['config']->get('smorken/ldap-auth::ldap.ldap_proxy');
                $provider_cls = $app['config']->get('smorken/ldap-auth::ldap.ldap_provider');
                $options = $app['config']->get('smorken/ldap-auth::ldap.provider_options', []);
                return new $cls($provider_cls, $options);
            }
        );
    }

    protected function bindUserProvider()
    {
        $this->app->bind(
            Provider::class,
            function ($app) {
                $proxy = $app[Proxy::class];
                $provider_class = $app['config']->get('smorken/ldap-auth::config.users.provider');
                $model = $this->setupUserModel();
                return new $provider_class($proxy, $model, $app[Repository::class]);
            }
        );
    }

    protected function setupUserModel()
    {
        $model = $this->app['config']['smorken/ldap-auth::config.users.model'];

        if (method_exists($model, 'setLoginField')) {
            $loginAttribute = $this->app['config']['smorken/ldap-auth::config.users.login_attribute'];

            forward_static_call_array(
                [$model, 'setLoginField'],
                [$loginAttribute]
            );
        }

        return $model;
    }
}
