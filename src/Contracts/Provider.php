<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/19/17
 * Time: 10:10 AM
 */

namespace Smorken\LdapAuth\Contracts;

use Illuminate\Contracts\Auth\UserProvider;

interface Provider extends UserProvider
{

}
