<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/19/17
 * Time: 10:03 AM
 */

namespace Smorken\LdapAuth\Contracts;

interface Proxy
{
    public function setProviderClass($provider);

    public function setProvider($provider);

    public function provider();

    public function findById($identifier);

    public function findByCredentials($credentials);

    public function findByUsername($username);

    public function getUsername($ldapinfo);

    public function convertToUserAttributes($ldapinfo, $groups = false);

    public function addGroupInfo($ldapinfo);

    public function authenticate($credentials);

    public function getDefaultMap();

    public function getFullMap();

    public function usernameAttribute();

    public function idAttribute();

    public function groupAttribute();

    public function setLoginField($loginName);

    public function setIdField($idField);

    public function find($criteria = []);

    public function getProxyAttributes($ldapinfo);
}
