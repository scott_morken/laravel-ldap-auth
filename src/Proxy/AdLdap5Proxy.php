<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 3/17/14
 * Time: 7:39 AM
 */

namespace Smorken\LdapAuth\Proxy;

use Adldap\Adldap;
use Adldap\Objects\Ldap\Entry;
use Smorken\Auth\Exceptions\AuthException;
use Smorken\LdapAuth\Contracts\Proxy;

/**
 * Class AdLdapProxy
 * @package Smorken\Storage\User\Proxy
 * Implements proxy to adLDAP package
 * create an conf/auth.php to override default options
 * return array(
 * 'ldap' => array(
 *  'ldap_options' => array(
 *   'account_suffix' => "@domain.local",
 *   'domain_controllers' => array("dc1.domain.local", "dc2.domain.local"), // An array of domains may be provided for load balancing.
 *   'base_dn' => 'DC=domain,DC=local',
 *   'admin_username' => 'user',
 *   'admin_password' => 'password',
 *   'use_ssl' => true, // If TLS is true this MUST be false.
 *   'use_tls' => false, // If SSL is true this MUST be false.
 *   'recursive_groups' => true,
 *  ),
 *  'username_field' => 'samaccountname',
 *  'group_field' => 'memberOf',
 *  'maptouser' => array(
 *    ... see AdLdapProxy::getDefaultMap
 *  ),
 *  'ldap_user_class' => 'Smorken\Model\Ldap\LdapUser',
 * ),
 * );
 */
class AdLdap5Proxy implements Proxy
{

    /**
     * @var \Adldap\Adldap
     */
    protected $provider;

    protected $providerClass = '\Adldap\Adldap';
    protected $providerOptions = [];

    protected $usernameAttribute;
    protected $idAttribute;
    protected $groupAttribute;

    protected $loginNameField = 'username';
    protected $idField = 'id';

    public function __construct($providerClass = null, $providerOptions = [])
    {
        if ($providerClass !== null) {
            $this->setProviderClass($providerClass);
        }
        $this->providerOptions = $providerOptions;

        $this->usernameAttribute = config('smorken/ldap-auth::ldap.username_attribute', 'samaccountname');
        $this->idAttribute = config('smorken/ldap-auth::ldap.id_attribute', 'uid');
        $this->groupAttribute = config('smorken/ldap-auth::ldap.group_attribute', 'memberOf');
    }

    public function setLoginField($loginName)
    {
        $this->loginNameField = $loginName;
    }

    public function setIdField($idField)
    {
        $this->idField = $idField;
    }

    public function provider()
    {
        if (!$this->provider) {
            $this->provider = new $this->providerClass($this->providerOptions);
        }
        return $this->provider;
    }

    public function usernameAttribute()
    {
        return $this->usernameAttribute;
    }

    public function idAttribute()
    {
        return $this->idAttribute;
    }

    public function groupAttribute()
    {
        return $this->groupAttribute;
    }

    public function setProviderClass($provider)
    {
        $this->providerClass = $provider;
    }

    public function setProvider($provider)
    {
        $this->provider = $provider;
    }

    public function findById($identifier)
    {
        $this->checkProvider();
        $personCategory = $this->provider()->getPersonFilter('category');
        $person = $this->provider()->getPersonFilter('person');
        $result = $this->provider()->search()
                       ->select([])
                       ->where($personCategory, '=', $person)
                       ->where('samaccounttype', '=', Adldap::ADLDAP_NORMAL_ACCOUNT)
                       ->where($this->idAttribute(), '=', $identifier)
                       ->first();
        if (!$result) {
            throw new AuthException("User [$identifier] not found.");
        }
        return $this->convertToUserObject($result);
    }

    protected function convertToUserObject($data)
    {
        return new \Adldap\Objects\User($data, $this->provider()->getLdapConnection());
    }

    public function findByCredentials($credentials)
    {
        $this->checkProvider();
        $auth = $this->authenticate($credentials);
        if ($auth) {
            return $this->findByUsername($credentials[$this->loginNameField]);
        }
        return false;
    }

    public function findByUsername($username)
    {
        $results = $this->provider()->user()->find($username);
        return $this->convertToUserObject($results);
    }

    public function find($criteria = [])
    {
        $entries = [];
        $filter = $this->getFilterArray($criteria);
        if ($filter) {
            $personCategory = $this->provider()->getPersonFilter('category');
            $person = $this->provider()->getPersonFilter('person');
            $searcher = $this->provider()->search()
                             ->select([])
                             ->where($personCategory, '=', $person)
                             ->where('samaccounttype', '=', Adldap::ADLDAP_NORMAL_ACCOUNT);
            foreach ($filter as $f) {
                $searcher->where($f[0], $f[1], $f[2]);
            }
            $entries = $this->parseEntriesIntoCollections($searcher->get());
        }
        return $entries;
    }

    protected function parseEntriesIntoCollections($entries)
    {
        $collections = [];
        foreach ($entries as $k => $entry) {
            if (is_int($k)) {
                $add = $this->convertToUserObject($entry);
                $collections[] = $add;
            }
        }
        return $collections;
    }

    protected function getFilter($criteria = [])
    {
        $appends = ['sn' => '*', 'givenname' => '*'];
        $mapfieldname = $this->getFilterMap();
        $filter = [];
        foreach ($criteria as $field => $value) {
            if (isset($mapfieldname[$field])) {
                $field = $mapfieldname[$field];
            }
            $t = sprintf(
                '(%s=%s)',
                $field,
                $value . (isset($appends[$field]) ? $appends[$field] : '')
            );
            $filter[] = $t;
        }
        return ($filter ? "(&" . implode('', $filter) . ')' : false);
    }

    protected function getFilterArray($criteria = [])
    {
        $filter = [];
        $operators = ['sn' => 'starts_with', 'givenname' => 'starts_with'];
        $mapfieldname = $this->getFilterMap();
        foreach ($criteria as $field => $value) {
            if (isset($mapfieldname[$field])) {
                $field = $mapfieldname[$field];
            }
            $t = [$field, (isset($operators[$field]) ? $operators[$field] : '='), $value];
            $filter[] = $t;
        }
        return $filter;
    }

    protected function getFilterMap()
    {
        return [
            'username'   => $this->usernameAttribute,
            'id'         => $this->idAttribute(),
            'last_name'  => 'sn',
            'first_name' => 'givenname',
        ];
    }

    public function getUsername($ldapinfo)
    {
        $un_field = strtolower($this->usernameAttribute());
        return $ldapinfo->$un_field;
    }

    public function convertToUserAttributes($ldapinfo, $groups = false)
    {
        $mapping = $this->getFullMap();
        $results = [];
        foreach ($mapping as $ldap => $model) {
            $ldap = strtolower($ldap);
            try {
                $value = $ldapinfo->$ldap;
                if ($ldap == $this->usernameAttribute()) {
                    $value = strtolower($value);
                }
                $results[$model] = $value;
            } catch (\Exception $e) {
                $results[$model] = null;
            }
        }
        if ($groups) {
            $results['groups'] = $this->addGroupInfo($ldapinfo);
        }
        return $results;
    }

    public function addGroupInfo($ldapinfo)
    {
        $groups = [];
        $g_field = strtolower($this->groupAttribute);
        if (is_array($ldapinfo->$g_field)) {
            foreach ($ldapinfo->$g_field as $group) {
                $groups[] = $this->getNameFromFullPath($group);
            }
        }
        return $groups;
    }

    protected function getNameFromFullPath($dn)
    {
        $expDn = explode(',', $dn);
        $first = reset($expDn);
        if (strtolower(substr($first, 0, 3)) == 'cn=') {
            return substr($first, 3);
        }
        return $dn;
    }

    public function authenticate($credentials)
    {
        $this->checkProvider();
        if (!isset($credentials[$this->loginNameField])) {
            throw new AuthException("[{$this->loginNameField}] was not found in the credentials.");
        }
        return $this->provider()->authenticate($credentials[$this->loginNameField], $credentials['password']);
    }

    /**
     * Returns complete field mapping with id and username
     * @return array
     */
    public function getFullMap()
    {
        $mapping = $this->getMapping();
        $mapping[$this->usernameAttribute()] = $this->loginNameField;
        $mapping[$this->idAttribute()] = $this->idField;
        return $mapping;
    }

    /**
     * Returns field mapping merging config with defaults
     * @return array
     */
    public function getMapping()
    {
        $default = $this->getDefaultMap();
        $overrides = config('smorken/ldap-auth::ldap.maptouser', []);
        return array_merge($default, $overrides);
    }

    public function getDefaultMap()
    {
        return [
            'givenName' => 'first_name',
            'sn'        => 'last_name',
            'mail'      => 'email',
        ];
    }

    private function checkProvider()
    {
        if (!$this->providerClass) {
            throw new ProxyException("No provider has been set.");
        }
    }

    public function getProxyAttributes($ldapinfo)
    {
        return $ldapinfo;
    }
}
