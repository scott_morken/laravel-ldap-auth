<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 3/17/14
 * Time: 7:39 AM
 */

namespace Smorken\LdapAuth\Proxy;

use LdapQuery\Contracts\Filter;
use Smorken\Auth\Exceptions\AuthDisplayableException;
use Smorken\LdapAuth\Contracts\Proxy;

/**
 * Class LdapQueryProxy
 * @package Smorken\Storage\User\Proxy
 * Implements proxy to LdapProxy package
 * configuration defaults
 * return [
 * 'ldap' => [
 * 'ldap_options' => [
 * 'host' => 'localhost',
 * 'port' => 389,
 * 'base_dn' => 'dc=domain,dc=org',
 * 'bind_user' => null,
 * 'bind_password' => null,
 * 'rebind' => true,
 * 'follow_referrals' => 1,
 * 'bind_filter' => '%s@domain.org',
 * 'client_options' => [
 * 'ssl' => false,
 * 'start_tls' => false,
 * 'default_port' => 389,
 * 'default_ssl_port' => 636,
 * 'default_ssl_prefix' => 'ldaps://',
 * 'default_prefix' => 'ldap://',
 * ],
 * 'filters' => [
 * 'person' => [
 * 'category' => 'objectCategory',
 * 'category_value' => 'person',
 * 'uid_attribute' => 'anr',
 * 'samaccounttype' => 805306368,
 * ],
 * ],
 * ],
 * 'username_field' => 'samaccountname',
 * 'group_field' => 'memberOf',
 * 'maptouser' => [
 * //... see AdLdapProxy::getDefaultMap
 * ],
 * 'ldap_user_class' => 'Smorken\Model\Ldap\LdapUser',
 * ],
 * ];
 */
class LdapQueryProxy implements Proxy
{

    /**
     * @var \LdapQuery\LdapQuery
     */
    protected $provider;

    protected $providerClass = '\LdapQuery\Opinionated\ActiveDirectory';
    protected $providerOptions = [];

    protected $usernameAttribute;
    protected $idAttribute;
    protected $groupAttribute;

    protected $loginNameField = 'username';
    protected $idField = 'id';

    public function __construct($providerClass = null, $providerOptions = [])
    {
        if ($providerClass !== null) {
            $this->setProviderClass($providerClass);
        }
        $this->providerOptions = $providerOptions;

        $this->usernameAttribute = config('smorken/ldap-auth::ldap.username_attribute', 'samaccountname');
        $this->idAttribute = config('smorken/ldap-auth::ldap.id_attribute', 'uid');
        $this->groupAttribute = config('smorken/ldap-auth::ldap.group_attribute', 'memberOf');
    }

    public function setLoginField($loginName)
    {
        $this->loginNameField = $loginName;
    }

    public function setIdField($idField)
    {
        $this->idField = $idField;
    }

    public function provider()
    {
        if (!$this->provider) {
            $p = new $this->providerClass($this->providerOptions);
            $this->provider = $p->getLdapQuery();
        }
        return $this->provider;
    }

    public function usernameAttribute()
    {
        return $this->usernameAttribute;
    }

    public function idAttribute()
    {
        return $this->idAttribute;
    }

    public function groupAttribute()
    {
        return $this->groupAttribute;
    }

    public function setProviderClass($provider)
    {
        $this->providerClass = $provider;
    }

    public function setProvider($provider)
    {
        $this->provider = $provider;
    }

    public function findById($identifier)
    {
        $this->checkProvider();
        $q = $this->createUserQuery();
        $q->where($this->idAttribute(), $this->provider()->escape($identifier));
        $result = $this->provider()->execute($q)->first();
        if (!$result) {
            throw new AuthDisplayableException("User [$identifier] not found.", 'Invalid username and/or password.');
        }
        return $result;
    }

    protected function createUserQuery()
    {
        $filters = $this->provider()->getBackend()->getOptionByKey('filters', []);
        $pf = array_get($filters, 'person', []);
        $q = $this->provider()->newQuery()->select([]);
        if (array_get($pf, 'category', false)) {
            $q->where(array_get($pf, 'category'), $this->provider()->escape(array_get($pf, 'category_type', 'person')));
        }
        if (array_get($pf, 'samaccounttype', false)) {
            $q->where('samaccounttype', $this->provider()->escape(array_get($pf, 'samaccounttype')));
        }
        return $q;
    }

    public function findByCredentials($credentials)
    {
        $this->checkProvider();
        $auth = $this->authenticate($credentials);
        if ($auth) {
            return $this->findByUsername($credentials[$this->loginNameField]);
        }
        return false;
    }

    public function findByUsername($username)
    {
        return $this->provider()->findUser($username);
    }

    public function find($criteria = [])
    {
        $entries = [];
        $filter = $this->getFilterArray($criteria);
        if ($filter) {
            $searcher = $this->createUserQuery();
            foreach ($filter as $f) {
                $searcher->where($f[0], $f[2], $f[1]);
            }
            $entries = $this->provider()->execute($searcher)->get();
        }
        return $entries;
    }

    protected function getFilter($criteria = [])
    {
        $appends = ['sn' => '*', 'givenname' => '*'];
        $mapfieldname = $this->getFilterMap();
        $filter = [];
        foreach ($criteria as $field => $value) {
            if (isset($mapfieldname[$field])) {
                $field = $mapfieldname[$field];
            }
            $t = sprintf(
                '(%s=%s)',
                $field,
                $this->provider()->escape($value . (isset($appends[$field]) ? $appends[$field] : ''))
            );
            $filter[] = $t;
        }
        return ($filter ? "(&" . implode('', $filter) . ')' : false);
    }

    protected function getFilterArray($criteria = [])
    {
        $filter = [];
        $operators = ['sn' => Filter::STARTS_WITH_FILTER, 'givenname' => Filter::STARTS_WITH_FILTER];
        $mapfieldname = $this->getFilterMap();
        foreach ($criteria as $field => $value) {
            if (isset($mapfieldname[$field])) {
                $field = $mapfieldname[$field];
            }
            $t = [$field, (isset($operators[$field]) ? $operators[$field] : Filter::EQUALS_FILTER), $value];
            $filter[] = $t;
        }
        return $filter;
    }

    protected function getFilterMap()
    {
        return [
            'username'   => $this->usernameAttribute,
            'id'         => $this->idAttribute(),
            'last_name'  => 'sn',
            'first_name' => 'givenname',
        ];
    }

    public function getUsername($ldapinfo)
    {
        $un_field = strtolower($this->usernameAttribute());
        return $ldapinfo->$un_field;
    }

    public function convertToUserAttributes($ldapinfo, $groups = false)
    {
        $mapping = $this->getFullMap();
        $results = [];
        foreach ($mapping as $ldap => $model) {
            $ldap = strtolower($ldap);
            try {
                $value = $ldapinfo->$ldap;
                if ($ldap == $this->usernameAttribute()) {
                    $value = strtolower($value);
                }
                $results[$model] = $value;
            } catch (\Exception $e) {
                $results[$model] = null;
            }
        }
        if ($groups) {
            $results['groups'] = $this->addGroupInfo($ldapinfo);
        }
        return $results;
    }

    public function addGroupInfo($ldapinfo)
    {
        $groups = [];
        $g_field = strtolower($this->groupAttribute);
        if (is_array($ldapinfo->$g_field)) {
            foreach ($ldapinfo->$g_field as $group) {
                $groups[] = $this->getNameFromFullPath($group);
            }
        }
        return $groups;
    }

    protected function getNameFromFullPath($dn)
    {
        $expDn = explode(',', $dn);
        $first = reset($expDn);
        if (strtolower(substr($first, 0, 3)) == 'cn=') {
            return substr($first, 3);
        }
        return $dn;
    }

    public function authenticate($credentials)
    {
        $this->checkProvider();
        if (!isset($credentials[$this->loginNameField])) {
            throw new AuthDisplayableException(
                "[{$this->loginNameField}] was not found in the credentials.",
                'Username not supplied'
            );
        }
        return $this->provider()->authenticate($credentials[$this->loginNameField], $credentials['password']);
    }

    /**
     * Returns complete field mapping with id and username
     * @return array
     */
    public function getFullMap()
    {
        $mapping = $this->getMapping();
        $mapping[$this->usernameAttribute()] = $this->loginNameField;
        $mapping[$this->idAttribute()] = $this->idField;
        return $mapping;
    }

    /**
     * Returns field mapping merging config with defaults
     * @return array
     */
    public function getMapping()
    {
        $default = $this->getDefaultMap();
        $overrides = config('smorken/ldap-auth::ldap.maptouser', []);
        return array_merge($default, $overrides);
    }

    public function getDefaultMap()
    {
        return [
            'givenName' => 'first_name',
            'sn'        => 'last_name',
            'mail'      => 'email',
        ];
    }

    private function checkProvider()
    {
        if (!$this->providerClass) {
            throw new ProxyException("No provider has been set.");
        }
    }

    public function getProxyAttributes($ldapinfo)
    {
        return $ldapinfo;
    }
}
