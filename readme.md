Add service provider line to config/app.php

```
'providers' => array(
    ...
    'Smorken\LdapAuth\LdapAuthServiceProvider',
```

Copy vendor config files

```
$ php artisan vendor:publish
```

