<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/11/14
 * Time: 9:34 AM
 */
namespace {
use Illuminate\Support\Facades\Config;
use Mockery as m;

class AdLdapProxyTest extends \PHPUnit\Framework\TestCase
{

    /**
     * Close mockery.
     *
     * @return void
     */
    public function tearDown()
    {
        m::close();
    }

    public function testConstructSetsAttributes()
    {
        list($proxy, $prov) = $this->setupProxy();
        $this->assertEquals('samaccountname', $proxy->usernameAttribute());
        $this->assertEquals('uid', $proxy->idAttribute());
        $this->assertEquals('memberOf', $proxy->groupAttribute());
    }

    public function setupProxy()
    {
        $prov = m::mock('adLDAP\adLDAP');
        $prov->shouldReceive('close');
        $proxy = m::mock(
            'Smorken\LdapAuth\Proxy\AdLdapProxy[provider]',
            [

            ]
        );
        $proxy->shouldReceive('provider')
              ->andReturn($prov);
        return [$proxy, $prov];
    }

    /**
     * @expectedException \Smorken\Auth\Exceptions\AuthException
     */
    public function testFindByIdThrowsUserNotFound()
    {
        list($proxy, $prov) = $this->setupProxy();
        $prov->shouldReceive('user')->once()->andReturn($prov);
        $prov->shouldReceive('find')
             ->once()
             ->with(false, 'uid', 1)
             ->andReturn([]);
        $proxy->findById(1);
    }

    public function testFindById()
    {
        list($proxy, $prov) = $this->setupProxy();
        $prov->shouldReceive('user')->once()->andReturn($prov);
        $prov->shouldReceive('find')
             ->once()
             ->with(false, 'uid', 1)
             ->andReturn([['bar' => 1]]);
        $this->assertEquals(['bar' => 1], $proxy->findById(1));
    }

    /**
     * @expectedException \Smorken\Auth\Exceptions\AuthException
     */
    public function testFindByCredentialsFailsOnMissingLoginField()
    {
        list($proxy, $prov) = $this->setupProxy();
        $creds = ['not-foo' => 'bar', 'password' => 'baz'];
        $proxy->findByCredentials($creds);
    }

    public function testFindByCredentials()
    {
        $creds = ['foo' => 'bar', 'password' => 'baz'];
        list($proxy, $prov) = $this->setupProxy();
        $proxy->setLoginField('foo');

        $prov->shouldReceive('authenticate')
             ->once()
             ->with($creds['foo'], $creds['password'])
             ->andReturn(true);
        $prov->shouldReceive('user')->once()->andReturn($prov);
        $prov->shouldReceive('infoCollection')
             ->once()
             ->with($creds['foo'], ['*'])
             ->andReturn(['bar' => 1]);

        $this->assertEquals(['bar' => 1], $proxy->findByCredentials($creds));
    }

    public function testFindByCredentialsFails()
    {
        $creds = ['foo' => 'bar', 'password' => 'baz'];
        list($proxy, $prov) = $this->setupProxy();
        $proxy->setLoginField('foo');

        $prov->shouldReceive('authenticate')
             ->once()
             ->with($creds['foo'], $creds['password'])
             ->andReturn(false);
        $prov->shouldReceive('user')->never();
        $prov->shouldReceive('infoCollection')
             ->never();

        $this->assertFalse($proxy->findByCredentials($creds));
    }

    public function testGetUsernameFromInfo()
    {
        $info = new \stdClass();
        $info->samaccountname = 'bar';
        list($proxy, $prov) = $this->setupProxy();
        $this->assertEquals('bar', $proxy->getUsername($info));
    }

    public function testConvertToUserAttributes()
    {
        $expected = [
            'first_name' => null,
            'last_name'  => null,
            'email'      => null,
            'username'   => 'bar',
            'id'         => 'baz',
        ];
        $info = new \stdClass();
        $info->samaccountname = 'bar';
        $info->uid = 'baz';
        list($proxy, $prov) = $this->setupProxy();
        $this->assertEquals($expected, $proxy->convertToUserAttributes($info));
    }

    public function testGetFullMapAddsIdAndLoginAttributes()
    {
        list($proxy, $prov) = $this->setupProxy();
        $map = $proxy->getFullMap();
        $this->assertArrayHasKey('samaccountname', $map);
        $this->assertArrayHasKey('uid', $map);
    }

    public function testGetMapping()
    {
        $expected = [
            'givenName' => 'first_name',
            'sn' => 'last_name',
            'mail' => 'email',
        ];
        list($proxy, $prov) = $this->setupProxy();
        $this->assertEquals($expected, $proxy->getMapping());
    }
}
}

namespace Smorken\LdapAuth\Proxy {
    function config($key, $value)
    {
        return $value;
    }
}
