<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/11/14
 * Time: 1:12 PM
 */
use Illuminate\Support\Facades\Validator;
use Mockery as m;
use Smorken\LdapAuth\Users\Eloquent\User;

class EloquentUserTest extends \PHPUnit\Framework\TestCase
{

    /**
     * Close mockery.
     *
     * @return void
     */
    public function tearDown()
    {
        m::close();
    }

    public function setUp()
    {
    }

    public function setupUser($partial)
    {
        return m::mock($partial);
    }

    public function testGetAuthIdentifier()
    {
        $user = new User;
        $user->id = 1;
        $this->assertEquals(1, $user->getAuthIdentifier());
    }

    public function testGetAuthPassword()
    {
        $user = new User;
        $user->password = 'foo';
        $this->assertEquals('foo', $user->getAuthPassword());
    }

    public function testLoginFieldDefault()
    {
        $user = new User;
        $this->assertEquals('username', $user->getLoginField());
    }

    public function testLoginFieldOverride()
    {
        $user = new User;
        $orig = $user->getLoginField();
        User::setLoginField('bar');
        $this->assertEquals('bar', $user->getLoginField());
        //set it back for remaining tests
        User::setLoginField($orig);
    }
}
