<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/11/14
 * Time: 1:41 PM
 */

namespace {

    use Mockery as m;

    class LdapProviderTest extends \PHPUnit\Framework\TestCase
    {

        /**
         * Close mockery.
         *
         * @return void
         */
        public function tearDown()
        {
            m::close();
        }

        public function setUp()
        {
        }

        public function testRunsProxySetupOnce()
        {
            list($provider, $proxy) = $this->setupLdapProvider('Smorken\LdapAuth\Providers\Ldap[createModel]');
            $user = m::mock('Smorken\LdapAuth\Users\Eloquent\User');
            $user->shouldReceive('getLoginField')
                 ->once()
                 ->andReturn('foo');
            $proxy->shouldReceive('setLoginField')
                  ->once()
                  ->with('foo');
            $user->shouldReceive('getKeyName')
                 ->once()
                 ->andReturn('bar');
            $proxy->shouldReceive('setIdField')
                  ->once()
                  ->with('bar');
            $provider->shouldReceive('createModel')
                     ->andReturn($user);
            $provider->proxy();
            $provider->proxy();
            $this->assertTrue(true);
        }

        public function setupLdapProvider($partial)
        {
            $proxy = m::mock('Smorken\LdapAuth\Proxy\ProxyInterface');
            $cache = m::mock('Illuminate\Cache\Repository');
            $provider = m::mock($partial, [$proxy]);
            return [$provider, $proxy, $cache];
        }

        public function testRetrieveById()
        {
            list($provider, $proxy) = $this->setupLdapProvider('Smorken\LdapAuth\Providers\Ldap[createModel]');
            $query = m::mock('StdClass');

            $user = m::mock('Smorken\LdapAuth\Users\Eloquent\User');

            $query->shouldReceive('find')
                  ->with(1)
                  ->andReturn($user);

            $user->shouldReceive('newQuery')
                 ->andReturn($query);
            $provider->shouldReceive('createModel')
                     ->andReturn($user);
            $result = $provider->retrieveById(1);
            $this->assertEquals($user, $result);
        }

        public function testRetrieveByIdWithCache()
        {
            list($provider, $proxy, $cache) = $this->setupLdapProvider('Smorken\LdapAuth\Providers\Ldap[createModel]');
            $provider->setCache($cache);

            $user = m::mock('Smorken\LdapAuth\Users\Eloquent\User');

            $cache->shouldReceive('remember')->once()->with('ldap/rid/1', 5, m::type('callable'))->andReturn($user);

            $result = $provider->retrieveById(1);
            $this->assertEquals($user, $result);
        }

        /**
         * @expectedException \Smorken\Auth\Exceptions\AuthException
         */
        public function testRetrieveByIdFails()
        {
            list($provider, $proxy) = $this->setupLdapProvider('Smorken\LdapAuth\Providers\Ldap[createModel]');
            $query = m::mock('StdClass');

            $user = m::mock('Smorken\LdapAuth\Users\Eloquent\User');

            $query->shouldReceive('find')
                  ->with(1)
                  ->andReturn(null);

            $user->shouldReceive('getLoginField')
                 ->once()
                 ->andReturn('username');
            $user->shouldReceive('getKeyName')
                 ->once()
                 ->andReturn('id');

            $proxy->shouldReceive('setLoginField')
                  ->once()
                  ->with('username');
            $proxy->shouldReceive('setIdField')
                  ->once()
                  ->with('id');
            $proxy->shouldReceive('findById')
                  ->once()
                  ->andReturn(false);
            $user->shouldReceive('newQuery')
                 ->andReturn($query);
            $provider->shouldReceive('createModel')
                     ->andReturn($user);
            $result = $provider->retrieveById(1);
            $this->assertEquals($user, $result);
        }

        /**
         * @expectedException \Smorken\Auth\Exceptions\AuthDisplayableException
         */
        public function testRetrieveByCredentialsFailsWithNoLogin()
        {
            list($provider, $proxy) = $this->setupLdapProvider('Smorken\LdapAuth\Providers\Ldap[createModel]');
            $user = m::mock('Smorken\LdapAuth\Users\Eloquent\User');
            $user->shouldReceive('getLoginField')
                 ->once()
                 ->andReturn('foo');
            $provider->shouldReceive('createModel')
                     ->once()
                     ->andReturn($user);

            $provider->retrieveByCredentials(['not-foo' => 'foo', 'password' => 'p']);
        }

        /**
         * @expectedException \Smorken\Auth\Exceptions\AuthDisplayableException
         */
        public function testRetrieveByCredentialsFailsWithNoMatch()
        {
            $creds = ['foo' => 'foo', 'password' => 'p'];
            list($provider, $proxy) = $this->setupLdapProvider('Smorken\LdapAuth\Providers\Ldap[createModel, proxy]');
            $user = m::mock('Smorken\LdapAuth\Users\Eloquent\User');
            $user->shouldReceive('getLoginField')
                 ->andReturn('foo');
            $provider->shouldReceive('proxy')
                     ->andReturn($proxy);
            $provider->shouldReceive('createModel')
                     ->andReturn($user);
            $proxy->shouldReceive('findByCredentials')
                  ->with($creds)
                  ->andReturn(null);
            $provider->retrieveByCredentials($creds);
        }

        public function testRetrieveByCredentials()
        {
            $loadeduser = m::mock('Smorken\LdapAuth\Users\Eloquent\User');

            $creds = ['foo' => 'foo', 'password' => 'p'];
            list($provider, $proxy) = $this->setupLdapProvider(
                'Smorken\LdapAuth\Providers\Ldap[createModel, proxy, updateUserFromLdap]'
            );
            $query = m::mock('StdClass');
            $query->shouldReceive('where')
                  ->with('foo', '=', 'foo')
                  ->andReturn($query);
            $query->shouldReceive('first')
                  ->andReturn($loadeduser);

            $user = m::mock('Smorken\LdapAuth\Users\Eloquent\User');
            $user->shouldReceive('getLoginField')
                 ->once()
                 ->andReturn('foo');
            $user->shouldReceive('newQuery')
                 ->once()
                 ->andReturn($query);
            $provider->shouldReceive('proxy')
                     ->once()
                     ->andReturn($proxy);
            $provider->shouldReceive('createModel')
                     ->once()
                     ->andReturn($user);
            $proxy->shouldReceive('findByCredentials')
                  ->once()
                  ->with($creds)
                  ->andReturn(['ldap_attributes']);
            $provider->shouldReceive('updateUserFromLdap')
                     ->andReturn($loadeduser);
            $this->assertEquals($loadeduser, $provider->retrieveByCredentials($creds));
        }

        public function testRetrieveByCredentialsCallsUpdateNotDirty()
        {
            $loadeduser = m::mock('Smorken\LdapAuth\Users\Eloquent\User');

            $creds = ['foo' => 'foo', 'password' => 'p'];
            list($provider, $proxy) = $this->setupLdapProvider('Smorken\LdapAuth\Providers\Ldap[createModel, proxy]');
            $query = m::mock('StdClass');
            $query->shouldReceive('where')
                  ->with('foo', '=', 'foo')
                  ->andReturn($query);
            $query->shouldReceive('first')
                  ->andReturn($loadeduser);

            $user = m::mock('Smorken\LdapAuth\Users\Eloquent\User');
            $user->shouldReceive('getLoginField')
                 ->once()
                 ->andReturn('foo');
            $user->shouldReceive('newQuery')
                 ->once()
                 ->andReturn($query);
            $provider->shouldReceive('proxy')
                     ->andReturn($proxy);
            $provider->shouldReceive('createModel')
                     ->once()
                     ->andReturn($user);
            $proxy->shouldReceive('findByCredentials')
                  ->once()
                  ->with($creds)
                  ->andReturn(['ldap_attributes']);
            $proxy->shouldReceive('convertToUserAttributes')
                  ->once()
                  ->with(['ldap_attributes'], false)
                  ->andReturn(['id' => 1, 'username' => 'baz']);
            $loadeduser->shouldReceive('getAttribute')
                       ->once()
                       ->with('id')
                       ->andReturn(1);
            $loadeduser->shouldReceive('getAttribute')
                       ->once()
                       ->with('username')
                       ->andReturn('baz');
            $proxy->shouldReceive('getProxyAttributes')->andReturn(['id' => 1, 'username' => 'baz']);
            $loadeduser->shouldReceive('setProxyAttributes')->with(['id' => 1, 'username' => 'baz']);
            $this->assertEquals($loadeduser, $provider->retrieveByCredentials($creds));
        }

        public function testRetrieveByCredentialsCallsUpdateDirty()
        {
            $loadeduser = m::mock('Smorken\LdapAuth\Users\Eloquent\User');

            $creds = ['foo' => 'foo', 'password' => 'p'];
            list($provider, $proxy) = $this->setupLdapProvider('Smorken\LdapAuth\Providers\Ldap[createModel, proxy]');
            $query = m::mock('StdClass');
            $query->shouldReceive('where')
                  ->with('foo', '=', 'foo')
                  ->andReturn($query);
            $query->shouldReceive('first')
                  ->andReturn($loadeduser);

            $user = m::mock('Smorken\LdapAuth\Users\Eloquent\User');
            $user->shouldReceive('getLoginField')
                 ->once()
                 ->andReturn('foo');
            $user->shouldReceive('newQuery')
                 ->once()
                 ->andReturn($query);
            $provider->shouldReceive('proxy')
                     ->times(3)
                     ->andReturn($proxy);
            $provider->shouldReceive('createModel')
                     ->once()
                     ->andReturn($user);
            $proxy->shouldReceive('findByCredentials')
                  ->once()
                  ->with($creds)
                  ->andReturn(['ldap_attributes']);
            $proxy->shouldReceive('convertToUserAttributes')
                  ->once()
                  ->with(['ldap_attributes'], false)
                  ->andReturn(['id' => 1, 'username' => 'baz']);

            $loadeduser->shouldReceive('getAttribute')
                       ->once()
                       ->with('id')
                       ->andReturn(1);
            $loadeduser->shouldReceive('getAttribute')
                       ->once()
                       ->with('username')
                       ->andReturn('baz-bat');
            $loadeduser->shouldReceive('setAttribute')
                       ->with('username', 'baz');
            $loadeduser->shouldReceive('save')
                       ->andReturn($loadeduser);
            $proxy->shouldReceive('getProxyAttributes')->andReturn(['id' => 1, 'username' => 'baz']);
            $loadeduser->shouldReceive('setProxyAttributes')->with(['id' => 1, 'username' => 'baz']);
            $this->assertEquals($loadeduser, $provider->retrieveByCredentials($creds));
        }

        public function testRetrieveByCredentialsCallsSave()
        {
            $creds = ['foo' => 'foo', 'password' => 'p'];
            list($provider, $proxy) = $this->setupLdapProvider('Smorken\LdapAuth\Providers\Ldap[createModel, proxy]');
            $query = m::mock('StdClass');
            $query->shouldReceive('where')
                  ->with('foo', '=', 'foo')
                  ->andReturn($query);
            $query->shouldReceive('first')
                  ->andReturn(null);

            $user = m::mock('Smorken\LdapAuth\Users\Eloquent\User');
            $user->shouldReceive('getLoginField')
                 ->once()
                 ->andReturn('foo');
            $user->shouldReceive('newQuery')
                 ->once()
                 ->andReturn($query);
            $provider->shouldReceive('proxy')
                     ->times(3)
                     ->andReturn($proxy);
            $provider->shouldReceive('createModel')
                     ->twice()
                     ->andReturn($user);
            $proxy->shouldReceive('findByCredentials')
                  ->once()
                  ->with($creds)
                  ->andReturn(['ldap_attributes']);
            $proxy->shouldReceive('convertToUserAttributes')
                  ->once()
                  ->with(['ldap_attributes'], false)
                  ->andReturn(['id' => 'foo', 'username' => 'bar']);

            $user->shouldReceive('fill')
                 ->once()
                 ->with(['id' => 'foo', 'username' => 'bar']);
            $user->shouldReceive('setAttribute')
                 ->once()
                 ->with('password', 'LDAP');
            $user->shouldReceive('save')
                 ->andReturn($user);
            $proxy->shouldReceive('getProxyAttributes')->andReturn(['id' => 1, 'username' => 'baz']);
            $user->shouldReceive('setProxyAttributes')->with(['id' => 1, 'username' => 'baz']);
            $this->assertEquals($user, $provider->retrieveByCredentials($creds));
        }
    }
}

namespace Smorken\LdapAuth\Providers {

    function config($key, $value)
    {
        return $value;
    }
}
